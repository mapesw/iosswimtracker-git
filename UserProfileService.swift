//
//  UserProfileService.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/8/23.
//

import Foundation

class UserProfileService {
    // singleton
    static let shared = UserProfileService()
    
    // instance variable for urlString
    var urlString: String
    
    init() {
#if DEBUG
        urlString = "http://localhost:8080/api/user?email="
#else
        urlString = "https://swim-server-a39ef0ff437c.herokuapp.com/api/user?email="
#endif
    }
    
    // get profile method
    func getUserProfile(email: String, completion: @escaping (Result<Data, Error>) -> Void) {
        debugLogger(message: "User Profile Service: getUserProfile method called")
        
        
        guard let url = URL(string: "\(self.urlString)\(email)") else {
            completion(.failure(NSError(domain: "Invalid URL", code: 1, userInfo: nil)))
            return
        }
        
        // data task
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(NSError(domain: "Invalid response", code: 2, userInfo: nil)))
                return
            }
            
            if httpResponse.statusCode == 404 {
                completion(.failure(NSError(domain: "User not found", code: 404, userInfo: nil)))
                return
            }
            
            // completion handler
            if let data = data {
                debugLogger(message: "UserProfileService: got data. Returning to UserProfileViewModel...")
                
                completion(.success(data))
            } else {
                completion(.failure(NSError(domain: "Data is null", code: 3, userInfo: nil)))
            }
        }
        task.resume()
    }
    func persistToken(token: String, completion: @escaping (Result<Data, Error>) -> Void) {
        
        debugLogger(message: "Persisting notification token...")
        
        
        // Get the data from UserDefaults
        let storedToken = UserDefaults.standard.string(forKey: "notificationToken") ?? "token error"
        let email = UserDefaults.standard.string(forKey: "email") ?? "email error"
#if DEBUG
        let fullURLString = "http://localhost:8080/api/user"
#else
        let fullURLString = "https://swim-server-a39ef0ff437c.herokuapp.com/api/user"
#endif
        // Create URL
        guard let url = URL(string: fullURLString) else {
            completion(.failure(NSError(domain: "Invalid URL", code: 1, userInfo: nil)))
            return
        }
        
        var request = URLRequest(url:url)
        request.httpMethod = "PUT"
        
        // Prepare payload
        let payload: [String: Any] = ["email": email, "token": storedToken]
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: payload, options: [])
        } catch {
            completion(.failure(error))
            return
        }
        
        // Set Content-Type header
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // Create URLSession data task
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Handle error
            if let error = error {
                completion(.failure(error))
                return
            }
            
            // Check HTTP response
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(NSError(domain: "Invalid response", code: 2, userInfo: nil)))
                return
            }
            
            // Check HTTP status code
            if httpResponse.statusCode == 404 {
                completion(.failure(NSError(domain: "User not found", code: 404, userInfo: nil)))
                return
            }
            
            // Completion handler
            if let data = data {
                debugLogger(message: "UserProfileService: Token persisted.")
                completion(.success(data))
            } else {
                completion(.failure(NSError(domain: "Data is null", code: 3, userInfo: nil)))
            }
        }
        
        // Start the task
        task.resume()
    }
}
