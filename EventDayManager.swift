//
//  EventDayManager.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/18/23.
//

import Foundation
import SwiftUI
import Combine

class EventDayManager: ObservableObject {
    @Published var isEventDay: Bool = false
    @Published var estimatedArrivalTime: Date?
    @Published var formattedEstimatedArrivalTime: String = ""
    
    static let shared = EventDayManager()
    
    var timeRemaining: (days: Int?, hours: Int?, minutes: Int?, seconds: Int?) = (nil, nil, nil, nil)
    
    // Buffer to hold last few ETA values for averaging
    private var lastETAs: [TimeInterval] = []
    private let maxBufferCount = 5
    
    init() {
        self.isEventDay = checkIfEventDay()
    }
    
    func checkIfEventDay() -> Bool {
        let calendar = Calendar.current
        
        var eventDateComponents = DateComponents()
        eventDateComponents.year = 2023
        eventDateComponents.month = 9
        eventDateComponents.day = 23
        eventDateComponents.timeZone = TimeZone(identifier: "America/New_York") // EST
        
        guard let eventDate = calendar.date(from: eventDateComponents) else {
            return false
        }
        
        let currentDate = Date()
        return calendar.isDate(currentDate, inSameDayAs: eventDate)
    }
    
    func updateTimeRemaining(year: Int, month: Int, day: Int, hour: Int, minute: Int) {
        let calendar = Calendar.current
        
        // Set the hard-coded future date based on parameters
        var futureDateComponents = DateComponents()
        futureDateComponents.year = year
        futureDateComponents.month = month
        futureDateComponents.day = day
        futureDateComponents.hour = hour
        futureDateComponents.minute = minute
        futureDateComponents.second = 0
        futureDateComponents.timeZone = TimeZone(identifier: "America/New_York") // EST
        
        guard let futureDate = calendar.date(from: futureDateComponents) else {
            return
        }
        
        let currentDate = Date()
        
        // Calculate the difference between the two dates
        let components = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: futureDate)
        
        self.timeRemaining = (components.day, components.hour, components.minute, components.second)
    }
    
    func updateEstimatedArrivalTime(ETA: TimeInterval) {  // ETA in seconds
        DispatchQueue.main.async {
            // Add new ETA to the buffer
            self.lastETAs.append(ETA)
            
            // Limit the buffer size
            if self.lastETAs.count > self.maxBufferCount {
                self.lastETAs.removeFirst()
            }
            
            // Calculate average ETA
            let averageETA = self.lastETAs.reduce(0, +) / Double(self.lastETAs.count)
            
            let currentDate = Date()
            if let arrivalDate = Calendar.current.date(byAdding: .second, value: Int(averageETA), to: currentDate) {
                self.estimatedArrivalTime = arrivalDate
            }
        }
    }
    
    
}
