import SwiftUI

struct LocationStatsView: View {
    @StateObject var locationManager = LocationManager.shared
    
    var body: some View {
        VStack(alignment: .leading) {  // Main VStack
            if locationManager.isTracking {
                // show distance to buoys
                BuoyView(locationManager: locationManager)
                VStack(alignment: .leading) {  // Nested VStack
                    HStack() {
                        Text("Lat:")
                            .font(.body)
                            .fontWeight(.bold)
                        Text(String(format: "%.9f", locationManager.latitude))
                            .foregroundColor(.blue)
                        Text("Lng:")
                            .font(.body)
                            .fontWeight(.bold)
                        Text(String(format: "%.9f", locationManager.longitude))
                            .foregroundColor(.blue)
                        Spacer()
                    }
                    .padding(.bottom, 2)
                }
                .padding(.top,2)
            } else {
                if !locationManager.isNearDestination {
                    HStack {
                        Text("Location tracking is off.")
                            .font(.title2)
                            .fontWeight(.bold)
                            .foregroundColor(.gray)
                        Spacer()
                    }
                    .padding(.top, 2)
                }
            }
        }
        .frame(maxWidth: .infinity)
    }
}
struct LocationStatsView_Previews: PreviewProvider {
    static var previews: some View {
        LocationStatsView()
    }
}
