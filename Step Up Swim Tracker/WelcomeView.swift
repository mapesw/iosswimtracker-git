
import SwiftUI

struct WelcomeView: View {
    @ObservedObject var viewModel = UserProfileViewModel()
    @State private var userEmail: String = ""
    @State private var error = false
    
    var body: some View {
        
        VStack(alignment: .leading) {
            Spacer()
            
            if viewModel.isSubmitting {
                ProgressView("Fetching...")
            } else {
                Text("FIND YOUR WAY HOME 2023")
                    .font(.title)
                    .multilineTextAlignment(.leading)
                    .fixedSize(horizontal: false, vertical: true)
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                Text("Enter your email to continue")
                    .font(.body)
                    .multilineTextAlignment(.leading)
                    .fixedSize(horizontal: false, vertical: true)
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                TextField("Enter email", text: $userEmail)
                    .textFieldStyle(CustomTextFieldStyle())
                
                if viewModel.noUserError || error {
                    Text("Please check email. Only registered swimmers & volunteers can use the app.")
                        .font(.subheadline)
                        .multilineTextAlignment(.leading)
                        .fixedSize(horizontal: false, vertical: true)
                        .fontWeight(.bold)
                        .foregroundColor(.red)
                }
                Button(action: {
                    if isValidEmail(userEmail) {
                        error = false
                        viewModel.getData(email: userEmail)
                    } else {
                        error = true
                    }
                }) {
                    ZStack {
                        Color.white.opacity(0.5)
                            .frame(maxWidth: .infinity)
                            .cornerRadius(10)
                        
                        Text("LET'S GO")
                            .font(.body)
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                    }
                    
                }
                .frame(height: 50)
                .cornerRadius(10)
//                Button("LET'S GO") {
//                    if isValidEmail(userEmail) {
//                        error = false
//                        viewModel.getData(email: userEmail)
//
//                    } else {
//                        error = true
//                    }
//                }
//                .padding()
//                .frame(maxWidth: .infinity)
//                .background(Color.white.opacity(0.5))
//                .font(.body)
//                .fontWeight(.bold)
//                .foregroundColor(.white)
//                .cornerRadius(10)
                
            }
        }
        .padding()
        .background(
            Image("buoy")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .edgesIgnoringSafeArea(.all)
        )
    }
}
struct CustomTextFieldStyle: TextFieldStyle {
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
            .padding(15)
            .background(Color.white.opacity(0.5))
            .cornerRadius(10)
            .foregroundColor(.black)
            .font(.system(size: 20))
            .autocorrectionDisabled()
            .frame(height: 45)
            .padding(.bottom, 10)
    }
}

func isValidEmail(_ email: String) -> Bool {
    let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
    return emailPredicate.evaluate(with: email)
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}
