//
//  BuoyView.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/21/23.
//

import SwiftUI
import CoreLocation

struct BuoyDistanceView: View {
    @ObservedObject var locationManager = LocationManager.shared
    
    var buoyId: Int
    
    var body: some View {
        if let distance = locationManager.buoyDistances[buoyId] {
            HStack {
                Text("Distance to buoy \(buoyId): ")
                    .fontWeight(.bold)
                    .font(.body)
                Text("\(distance, specifier: "%.2f") meters")
                    .foregroundColor(.blue)
                Spacer()
            }
            .padding(.bottom,2)
            
        } else {
            Text("Calculating distance to buoy \(buoyId)...")
        }
    }
    
//    var currentLocation: CLLocation {
//            return CLLocation(latitude: locationManager.latitude, longitude: locationManager.longitude)
//        }
//
//    var remainingDistance: CLLocationDistance {
//            return locationManager.distanceRemaining(currentLocation: currentLocation)
//        }
}


struct BuoyView: View {
    @ObservedObject var locationManager: LocationManager
    
    var body: some View {
        VStack {
            VStack {
                ForEach(locationManager.buoys, id: \.id) { buoy in
                    BuoyDistanceView(locationManager: locationManager, buoyId: buoy.id)
                }
            }
            
            HStack {
                Text("Distance To Lighthouse: ")
                    .fontWeight(.bold)
                    .font(.body)
                Text("\(String(format: "%.2f", remainingDistance)) meters")
                    .foregroundColor(.blue)
                Spacer()
            }
        }
    }
    
    var currentLocation: CLLocation {
        return CLLocation(latitude: locationManager.latitude, longitude: locationManager.longitude)
    }
    
    var remainingDistance: Double {
        return locationManager.distanceRemaining(currentLocation: currentLocation)
    }
}

struct BuoyView_Previews: PreviewProvider {
    static var previews: some View {
        let locationManager = LocationManager.shared
        locationManager.buoys = [
            Buoy(id: 1, coordinate: CLLocationCoordinate2D(latitude: 42.06781666666667, longitude: -83.1529)),
            Buoy(id: 2, coordinate: CLLocationCoordinate2D(latitude: 42.05097, longitude: -83.15142)),
            Buoy(id: 3, coordinate: CLLocationCoordinate2D(latitude: 42.03405, longitude: -83.14998)),
            Buoy(id: 4, coordinate: CLLocationCoordinate2D(latitude: 42.01753, longitude: -83.14583))
        ]
        return BuoyView(locationManager: locationManager)
    }
}
