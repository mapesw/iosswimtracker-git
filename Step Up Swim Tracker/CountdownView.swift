//
//  CountdownView.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/18/23.
//

import SwiftUI

struct CountdownView: View {
    @State private var timeRemaining: (days: Int?, hours: Int?, minutes: Int?, seconds: Int?) = (nil, nil, nil, nil)
    @State private var timer: Timer? = nil
    @ObservedObject var eventDayManager = EventDayManager.shared
    
    func startTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
            eventDayManager.updateTimeRemaining(year: 2023, month: 9, day: 22, hour: 8, minute: 00)
            self.timeRemaining = (eventDayManager.timeRemaining.days ?? 0,
                                  eventDayManager.timeRemaining.hours ?? 0,
                                  eventDayManager.timeRemaining.minutes ?? 0,
                                  eventDayManager.timeRemaining.seconds ?? 0)
        }
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            
            Text("Event Starting In:")
                .font(.title2)
                .fontWeight(.bold)
                .padding(.bottom, 2)
            
            HStack {
                Text("\(timeRemaining.days ?? 0)").font(.title).fontWeight(.bold)
                Text("days").foregroundColor(Color.gray)
                
                Text("\(timeRemaining.hours ?? 0)").font(.title).fontWeight(.bold)
                Text("hrs").foregroundColor(Color.gray)
                
                Text("\(timeRemaining.minutes ?? 0)").font(.title).fontWeight(.bold)
                Text("min").foregroundColor(Color.gray)
                
                Text("\(timeRemaining.seconds ?? 0)").font(.title).fontWeight(.bold)
                Text("sec").foregroundColor(Color.gray)
                
                Spacer()
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .onAppear {
                eventDayManager.updateTimeRemaining(year: 2023, month: 9, day: 20, hour: 8, minute: 0)
                startTimer()
            }
            .onDisappear {
                timer?.invalidate()
            }
            
        }
    }
}
struct CountdownView_Previews: PreviewProvider {
    static var previews: some View {
        CountdownView()
    }
}
