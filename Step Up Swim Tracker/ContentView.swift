//
//  ContentView.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 8/16/23.
//

import SwiftUI
import UserNotifications

struct ContentView: View {
    @UIApplicationDelegateAdaptor(NotificationDelegate.self) var delegate
    @AppStorage("userVerified") var userVerified: Bool = false
    
    var body: some View {
        if (userVerified) {             
            NavMapView()
                .onAppear(perform: {
                    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in
                        print("Permission completion block called with granted: \(granted) and error: \(error?.localizedDescription ?? "None")")
                        if granted {
                            print("registering for remote notifications...")
                            DispatchQueue.main.async {
                                UIApplication.shared.registerForRemoteNotifications()
                            }
                        }
                    }
                    
                })
            
            //            .toolbarBackground(.black)
        } else {
            WelcomeView()
            
        }
        
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
