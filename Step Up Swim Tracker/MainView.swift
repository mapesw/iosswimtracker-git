//
//  MainView.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 8/16/23.
//

import SwiftUI

struct MainView: View {
    @StateObject var locationManager = LocationManager.shared
    @ObservedObject var eventDayManager = EventDayManager.shared
    @ObservedObject var socketManager = SocketIOManager.shared
    @State private var isTracking = false
    @State private var showAlert = false
    @Environment(\.colorScheme) var colorScheme
    
    var body: some View {
        
        NavMapView()
        
    }
}
struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
