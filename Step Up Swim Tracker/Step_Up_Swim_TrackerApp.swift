//
//  Step_Up_Swim_TrackerApp.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 8/16/23.
//

import SwiftUI

@main
struct Step_Up_Swim_TrackerApp: App {
    @UIApplicationDelegateAdaptor(NotificationDelegate.self) var delegate
    
    init() {}
    
    var body: some Scene {
        WindowGroup {
            ContentView()
            
        }
        
    }
}
