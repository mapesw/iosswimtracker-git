//
//  NavMapView.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/21/23.
//

import SwiftUI


struct NavMapView: View {
    @State private var selection: Int? = nil
    @StateObject var locationManager = LocationManager.shared
    @ObservedObject var eventDayManager = EventDayManager.shared
    @ObservedObject var socketManager = SocketIOManager.shared
    @State private var isTracking = false
    @State private var showAlert = false
    @State private var showMap = false
    @Environment(\.colorScheme) var colorScheme
    
    var body: some View {
        ZStack {
            VStack {
                // Your main content here
                StatsView()
            }
            
            if showMap {
                BuoyMapView()
                    .edgesIgnoringSafeArea(.all)
                    .transition(.opacity)
            }
        }
        .toolbar {
            ToolbarItem(placement: .bottomBar) {
                Button(action: {
                    withAnimation {
                        showMap.toggle()
                    }
                }) {
                    HStack {
                        if showMap {
                            Image(systemName: "chart.bar")
                            Text("Stats")
                        } else {
                            Image(systemName: "map")
                            Text("Map")
                        }
                    }
                }
            }
        }
    }
}

struct NavMapView_Previews: PreviewProvider {
    static var previews: some View {
        NavMapView()
    }
}
