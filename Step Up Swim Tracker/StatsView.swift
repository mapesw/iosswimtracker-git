//
//  StatsView.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/21/23.
//

import SwiftUI

struct StatsView: View {
    @StateObject var locationManager = LocationManager.shared
    @ObservedObject var eventDayManager = EventDayManager.shared
    @ObservedObject var socketManager = SocketIOManager.shared
    @State private var isTracking = false
    @State private var showAlert = false
    @Environment(\.colorScheme) var colorScheme
    
    private var isSmallScreen: Bool {
        
            return UIScreen.main.bounds.width < 350
        }
    
    var body: some View {
        VStack(){
            Spacer()
            
            Text("FIND YOUR WAY HOME 2023")
                .padding(.bottom, isSmallScreen ? 10 : 20)
                .font(.system(size: isSmallScreen ? 16 : 20))
                .multilineTextAlignment(.leading)
                .fixedSize(horizontal: false, vertical: true)
                .fontWeight(.bold)
            
            if colorScheme == .dark {
                Image("logo_white")
                    .resizable()
                    .scaledToFit()
                    .frame(width: isSmallScreen ? 100 : 200 )
            } else {
                Image("logo_black")
                    .resizable()
                    .scaledToFit()
                    .frame(width: isSmallScreen ? 100 : 200 )
            }
            
            Text("#LIVEBIG")
                .padding(.bottom)
                .font(.title)
                .multilineTextAlignment(.leading)
                .fixedSize(horizontal: false, vertical: true)
                .fontWeight(.bold)
                .foregroundColor(.blue)
            
            Spacer()
            
//            if !eventDayManager.isEventDay {
//                CountdownView()
//            }
            
            if locationManager.isTracking {
                SwimmingView()
            }
            
            
            //            LocationStatsView()
            
            if (!locationManager.isNearDestination) {
                
                if (locationManager.isTracking) {
                    Button(action: {
                        locationManager.stopLocation()
                    }) {
                        ZStack {
                            Color.red.opacity(0.75)
                                .frame(maxWidth: .infinity)
                                .cornerRadius(10)
                            
                            Text("Stop Tracking")
                                .font(.body)
                                .fontWeight(.bold)
                                .foregroundColor(.white)
                        }
                    }
                    .frame(height: 50)
                    .cornerRadius(10)
                } else {
                    Button(action: {
                        
                            locationManager.locationManager.startUpdatingLocation()

                    }) {
                        ZStack {
                            if eventDayManager.isEventDay {
                                Color.green.opacity(0.75)
                                    .frame(maxWidth: .infinity)
                                    .cornerRadius(10)
                            } else {
                                Color.gray.opacity(0.75)
                                    .frame(maxWidth: .infinity)
                                    .cornerRadius(10)
                            }
                            Text("Start Tracking")
                                .font(.body)
                                .fontWeight(.bold)
                                .foregroundColor(.white)
                        }
                    }
                    .frame(height: 50)
                    .cornerRadius(10)
                    //                .disabled(!eventDayManager.isEventDay)
                }
            }
            
        }
        .padding()
    }
}

struct StatsView_Previews: PreviewProvider {
    static var previews: some View {
        StatsView()
    }
}
