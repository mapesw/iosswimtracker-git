//
//  SwimmingView.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/19/23.
//

import SwiftUI
import CoreLocation

struct SwimmingView: View {
    @ObservedObject var locationManager = LocationManager.shared
    @ObservedObject var eventDayManager = EventDayManager.shared
    
    static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        formatter.timeZone = TimeZone(identifier: "America/New_York") // EST
        return formatter
    }()
    
    var estimatedArrivalText: some View {
        Group {
            if let estimatedArrival = eventDayManager.estimatedArrivalTime {
                // Convert Date to seconds since now
                let secondsFromNow = estimatedArrival.timeIntervalSinceNow
                Text("\(locationManager.formatETA(seconds: secondsFromNow))")
                    .font(.title2)
                    .fontWeight(.bold)
                    .padding(.bottom, 2)
                    .foregroundColor(.blue)
            } else {
                Text("Available when tracking started.")
                    .font(.title2)
                    .fontWeight(.bold)
                    .padding(.bottom, 2)
                    .foregroundColor(.gray)
            }
        }
    }
    
    var body: some View {
        if eventDayManager.isEventDay {
            VStack(alignment: .leading) {
                
                let currentLocation = CLLocation(latitude: locationManager.latitude, longitude: locationManager.longitude)
                
                let remainingDistance = locationManager.distanceRemaining(currentLocation: currentLocation)
                let eta = locationManager.estimatedTimeArrival(distanceRemaining: remainingDistance)
                
                if locationManager.isNearDestination {
                    VStack () {
                        HStack {
                            Spacer()
                            Text("YOU DID IT!")
                                .font(.title)
                                .fontWeight(.bold)
                                .foregroundColor(.blue)
                            Spacer()
                        }
                        HStack {
                            Spacer()
                            Text("SEE YOU NEXT YEAR!")
                                .font(.title)
                                .fontWeight(.bold)
                                .foregroundColor(.blue)
                            Spacer()
                        }
                        
                    }
                    
                } else {
                    
                    HStack {
                        Text("Lighthouse ETA: ")
                            .font(.title)
                            .fontWeight(.bold)
                        Spacer()
                    }
                    
                    HStack {
                        estimatedArrivalText
                        
                        Spacer()
                    }
                    .padding(.bottom,2)
                    
                    
//                    HStack {
//                        Text("Speed: ")
//                            .fontWeight(.bold)
//                            .font(.body)
//                        Text("\(String(format: "%.2f", locationManager.speed)) m/s")
//                            .foregroundColor(.blue)
//                        Spacer()
//                    }
//                    .padding(.bottom,2)
//                    HStack {
//                        Text("Distance To Light: ")
//                            .fontWeight(.bold)
//                            .font(.body)
//                        Text("\(String(format: "%.2f", remainingDistance)) meters")
//                            .foregroundColor(.blue)
//                        Spacer()
//                    }
                    .padding(.bottom,2)
                    LocationStatsView()
                    HStack {
                        Text("Distance Traveled: ")
                            .fontWeight(.bold)
                            .font(.body)
                        Text("\(String(format: "%.2f", locationManager.totalDistance)) meters")
                            .foregroundColor(.blue)
                        Spacer()
                    }
                    .padding(.bottom,2)
                    
                    
                }
            }
            .frame(maxWidth: .infinity)
            
            
        }
    }
    
}



func timeString(time: Double) -> String {
    let time = Int(time)
    let hours = time / 3600
    let minutes = (time % 3600) / 60
    let seconds = (time % 3600) % 60
    return String(format: "%02d hrs: %02d min: %02d sec", hours, minutes, seconds)
}

struct SwimmingView_Previews: PreviewProvider {
    static var previews: some View {
        SwimmingView()
    }
}
