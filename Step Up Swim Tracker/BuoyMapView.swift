import SwiftUI
import MapKit

struct Buoy: Identifiable {
    let id: Int
    let coordinate: CLLocationCoordinate2D
}

struct Lighthouse {
    let coordinate: CLLocationCoordinate2D
}

struct YachtClub {
    let coordinate: CLLocationCoordinate2D
}

struct BuoyMapView: View {
    @StateObject var locationManager = LocationManager.shared
    @State private var region: MKCoordinateRegion
    let buoys: [Buoy]
    let lighthouse: Lighthouse
    let yachtClub: YachtClub
    
    init() {
        
        self.buoys = [
            Buoy(id: 1, coordinate: CLLocationCoordinate2D(latitude: 42.06781666666667, longitude: -83.1529)),
            Buoy(id: 2, coordinate: CLLocationCoordinate2D(latitude: 42.05097, longitude: -83.15142)),
            Buoy(id: 3, coordinate: CLLocationCoordinate2D(latitude: 42.03405, longitude: -83.14998)),
            Buoy(id: 4, coordinate: CLLocationCoordinate2D(latitude: 42.01753, longitude: -83.14583))
        ]
        
        self.lighthouse = Lighthouse(coordinate: CLLocationCoordinate2D(latitude: 42.00229938521246, longitude: -83.14036143170868))
        self.yachtClub = YachtClub(coordinate: CLLocationCoordinate2D(latitude: 42.089721880040976, longitude: -83.15229681956541))
        
        let initialLocation = buoys.first?.coordinate ?? CLLocationCoordinate2D(latitude: 42.0, longitude: -83.0)
        _region = State(initialValue: MKCoordinateRegion(center: initialLocation, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)))
    }
    
    var body: some View {
        Map(coordinateRegion: $region, showsUserLocation: true,
            annotationItems: buoys + [Buoy(id: 0, coordinate: lighthouse.coordinate), Buoy(id: -1, coordinate: yachtClub.coordinate)]) { buoy in
            MapMarker(coordinate: buoy.coordinate, tint: colorForBuoy(buoy))
        }
            .edgesIgnoringSafeArea(.all)
            .onAppear{
                if !locationManager.isTracking {
                    locationManager.startLocation()
                }
            }
        
    }
    
    
    func colorForBuoy(_ buoy: Buoy) -> Color {
        switch buoy.id {
        case 0:
            return .red // lighthouse
        case -1:
            return .green // yacht club
        default:
            return .blue // regular buoys
        }
    }
}

struct BuoyMapView_Previews: PreviewProvider {
    static var previews: some View {
        BuoyMapView()
    }
}
