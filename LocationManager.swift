//
//  Location.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 8/18/23.
//

import Foundation
import CoreLocation
import Starscream
import MapKit

// Location Manager
class LocationManager: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    // Singleton instance
    static let shared = LocationManager()
    
    // EventDayManager
    var eventDayManager = EventDayManager.shared
    
    var locationManager = CLLocationManager()
    
    @Published var buoys: [Buoy] = [
        Buoy(id: 1, coordinate: CLLocationCoordinate2D(latitude: 42.06781666666667, longitude: -83.1529)),
        Buoy(id: 2, coordinate: CLLocationCoordinate2D(latitude: 42.05097, longitude: -83.15142)),
        Buoy(id: 3, coordinate: CLLocationCoordinate2D(latitude: 42.03405, longitude: -83.14998)),
        Buoy(id: 4, coordinate: CLLocationCoordinate2D(latitude: 42.01753, longitude: -83.14583))
    ]
    
    @Published var region = MKCoordinateRegion(
            center: .init(latitude: 42.00042295710168, longitude: -83.14047442210949),
            span: .init(latitudeDelta: 0.2, longitudeDelta: 0.2)
        )
    
    @Published var buoyDistances: [Int: CLLocationDistance] = [:]
    @Published var authorizationStatus: CLAuthorizationStatus?
    @Published var latitude: Double = 0.0
    @Published var longitude: Double = 0.0
    @Published var speed: Double = 0.0
    @Published var heading: Double = 0.0
    @Published var isTracking:Bool = false
    @Published var totalDistance:Double = 0.0
    @Published var lastLocation: CLLocation?
    @Published var isNearDestination: Bool = false
    
    let lighthouse = CLLocation(latitude: 42.00042295710168, longitude: -83.14047442210949)
    var speedMeasurements: [Double] = []
    
    var hasSentArrivalMessage = false
    
    override init() {
        super.init()
        locationManager.delegate = self
        
        // enable background location updates
        locationManager.allowsBackgroundLocationUpdates = true
        
        // show indicator when tracking
        locationManager.showsBackgroundLocationIndicator = true
        
        // accuracy
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        //don't pause location
        locationManager.pausesLocationUpdatesAutomatically = false
        
        
        // distance filter
        //        locationManager.distanceFilter = 5
        
        
    }
    
    // calc distance remaining
    func distanceRemaining(currentLocation: CLLocation) -> Double {
        // Calculate distance to each buoy and sum them up
        return currentLocation.distance(from: lighthouse)
    }
    
    // Your existing function where you calculate ETA
    func estimatedTimeArrival(distanceRemaining: Double) -> Double {
        let recentSpeeds = speedMeasurements.suffix(5) // last 5 measurements
        let averageSpeed = recentSpeeds.reduce(0.0, +) / Double(recentSpeeds.count)
        
        // Use averageSpeed instead of immediate speed to calculate ETA
        if averageSpeed > 0 {
            let etaInSeconds = distanceRemaining / averageSpeed
            let formattedETA = formatETA(seconds: etaInSeconds)
            
            eventDayManager.updateEstimatedArrivalTime(ETA: etaInSeconds)
            
            DispatchQueue.main.async {
                self.eventDayManager.formattedEstimatedArrivalTime = formattedETA // Set the formatted ETA here
            }
            
            return etaInSeconds
        }
        return 0.0
    }
    
    // format eta
    func formatETA(seconds: Double) -> String {
        let now = Date()
        let etaDate = now.addingTimeInterval(seconds)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a" // non-military time with AM/PM
        formatter.timeZone = TimeZone(identifier: "America/New_York") // EST
        
        return formatter.string(from: etaDate)
    }
    
    
    // Function to update totalDistance (Call this whenever location updates)
    func updateTotalDistance(newLocation: CLLocation, lastLocation: CLLocation) {
        let distance = newLocation.distance(from: lastLocation)
        totalDistance += distance
    }
    
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
            
        case .authorizedWhenInUse:
            debugLogger(message:"LOCATIONMANAGER: Location authorized - when in use")
            //            locationManager.requestLocation()
            //            locationManager.startUpdatingLocation()
            
            
        case .authorizedAlways:
            debugLogger(message: "LOCATIONMANAGER: Location authorized - always")
            authorizationStatus = .authorizedAlways
            //            locationManager.requestLocation() //one time location call
            //            locationManager.startUpdatingLocation() //ongoing
            break
            
        case .restricted:
            debugLogger(message: "LOCATIONMANAGER: Location restricted")
            authorizationStatus = .restricted
            break
            
        case .denied:
            debugLogger(message: "LOCATIONMANAGER: Location denied")
            authorizationStatus = .denied
            break
            
        case .notDetermined:
            debugLogger(message: "LOCATIONMANAGER: Location not determined. Asking for authorization.")
            authorizationStatus = .notDetermined
            manager.requestAlwaysAuthorization()
            break
            
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            
            // notify SocketIO delegate of change
            // delegate?.didStartTrackingLocation()
            
            // tracking location flag
            self.isTracking = true
            
            // Update location attributes
            latitude = location.coordinate.latitude
            longitude = location.coordinate.longitude
            speed = location.speed
            
            // add speed to avg array
            speedMeasurements.append(speed)
            if speedMeasurements.count > 5 {
                speedMeasurements.removeFirst()
            }
            // heading
            heading = location.course
            
            // update distance to buoys
            updateBuoyDistances(from: location)
            
            // persist location on device
            UserDefaults.standard.set(latitude, forKey: "latitude")
            UserDefaults.standard.set(longitude, forKey: "longitude")
            UserDefaults.standard.set(speed, forKey: "speed")
            UserDefaults.standard.set(heading, forKey: "heading")
            
            // send data to server
            NetworkManager.shared.postLocationData {(data, error) in
                if data != nil {
                    debugLogger(message: "posted JSON")
                }
                
                if let error = error {
                    debugLogger(message: "error: \(error)")
                }
            }
            
            // Calculate remaining distance to the buoys
            let remainingDistance = distanceRemaining(currentLocation: location)
            
            isNearDestination = remainingDistance < 5
            
            
            // send a socket message when we reach the destination
            if isNearDestination && !hasSentArrivalMessage {
                debugLogger(message: "Arrived at destination!")
                
                var userInfo: [String:Any] = [:]
                userInfo["userId"] = UserDefaults.standard.string(forKey: "userId")
                
                SocketIOManager.shared.socket.emit("arrivedAtDestination",userInfo)
                hasSentArrivalMessage = true
                self.stopLocation()
            }
            
            
            // Calculate estimated time of arrival based on speed and remaining distance
            let eta = estimatedTimeArrival(distanceRemaining: remainingDistance)
            
            
            eventDayManager.updateEstimatedArrivalTime(ETA: eta)
            
            // Update the total distance if last location is available
            if let lastLoc = self.lastLocation {
                updateTotalDistance(newLocation: location, lastLocation: lastLoc)
            }
            
            // Update last location for future reference
            self.lastLocation = location
            
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        debugLogger(message: "Failed to update location")
        debugLogger(message: "\(error)")
    }
    
    func isDeviceLocationEnabled() -> Bool {
        return CLLocationManager.locationServicesEnabled()
    }
    
    private func updateBuoyDistances(from currentLocation: CLLocation) {
        for buoy in buoys {
            let buoyLocation = CLLocation(latitude: buoy.coordinate.latitude, longitude: buoy.coordinate.longitude)
            let distance = currentLocation.distance(from: buoyLocation)
            buoyDistances[buoy.id] = distance
        }
    }
    
    func stopLocation() {
        
        locationManager.stopUpdatingLocation()
        debugLogger(message: "LocationManager: stopped location tracking")
        
        // UI Flag
        self.isTracking = false
        
    }
    
    func startLocation() {
        
        locationManager.startUpdatingLocation()
        debugLogger(message: "LocationManager: Started updating locations...")
        
        self.isTracking = true
        
        //        delegate?.didStartTrackingLocation()
    }
    
    
}
