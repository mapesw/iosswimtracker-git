//
//  UserModel.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/8/23.
//

import Foundation

struct Member: Codable {
    let id: String
    let email: String
    let name: String
    let vessel: String
    let createdAt: String
    let updatedAt: String
    let teamId: String
}

struct Team: Codable {
    let id: String
    let name: String
    let createdAt: String
    let updatedAt: String
    let members: [Member]
}

struct UserProfile: Codable {
    let id: String
    let email: String
    let name: String
    let vessel: String
    let createdAt: String
    let updatedAt: String
    let teamId: String?
    let team: Team?
    let message: String?
    let individual: Bool
    let status: String
}
