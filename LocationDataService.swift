//
//  LocationDataService.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/9/23.
//

import Foundation

func getLocationDataFromMemory(completion: @escaping(Result<LocationDataModel, Error>) -> Void ) {
    guard let userId = UserDefaults.standard.string(forKey: "userId"),
          let name = UserDefaults.standard.string(forKey: "name"),
          let vessel = UserDefaults.standard.string(forKey: "vessel") else {
        completion(.failure(LocationDataError.missingData))
        return
    }
    
    let latitude = UserDefaults.standard.double(forKey: "latitude")
    let longitude = UserDefaults.standard.double(forKey: "longitude")
    let speed = UserDefaults.standard.double(forKey: "speed")
    let heading = UserDefaults.standard.double(forKey: "heading")
    
    let location = LocationDataModel(userId: userId, name: name, vessel: vessel, latitude: latitude, longitude: longitude, speed: speed, heading: heading)
    
    completion(.success(location))
}
