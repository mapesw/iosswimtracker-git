//
//  UserProfileViewModel.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/8/23.
//

import Foundation

class UserProfileViewModel: ObservableObject {
    @Published var isSubmitting = false
    @Published var noUserError = false
    
    func getData(email: String) {
        debugLogger(message: "UserProfileViewModel: Called get data...")
        
        // set isSubmitting flag...
        self.isSubmitting = true
        
        // get data from UserProfile Service
        UserProfileService.shared.getUserProfile(email: email) { result in
            DispatchQueue.main.async {
                
                // set isSubmitting flag when result
                self.isSubmitting = false
                
                switch result {
                case .success(let data):
                    debugLogger(message: "UserProfileViewModel: Got profile data. Setting UserDefaults...")
                    
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any],
                           let email = json["email"] as? String,
                           let vessel = json["vessel"] as? String,
                           let name = json["name"] as? String,
                           let userId = json["id"] as? String {
                            UserDefaults.standard.set(name, forKey: "name")
                            UserDefaults.standard.set(userId, forKey: "userId")
                            UserDefaults.standard.set(vessel, forKey: "vessel")
                            UserDefaults.standard.set(email, forKey: "email")
                        }
                    } catch let error {
                        debugLogger(message: "JSON decoding error: \(error)")
                    }
                    
                    
                    // Handle success and store in UserDefaults
                    UserDefaults.standard.set(data, forKey: "RawUserProfile")
                    
                    UserDefaults.standard.set(true, forKey: "userVerified")
                    
                case .failure(let error):
                    if (error as NSError).code == 404 {
                        self.noUserError = true
                    }
                    debugLogger(message: "error \(error)")
                }
            }
        }
    }
}
