//
//  LocationDataModel.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/9/23.
//

import Foundation

struct LocationDataModel: Codable {
    let userId: String
    let name: String
    let vessel: String
    let latitude: Double
    let longitude: Double
    let speed: Double
    let heading: Double
}

enum LocationDataError: Error {
    case missingData
}
