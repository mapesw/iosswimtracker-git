//
//  Utils.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 8/16/23.
//

import Foundation
import CoreLocation
import Combine

// debug logger

func debugLogger(message:String) {
    
    print("DEBUG LOG MESSAGE: \(message)")
    
}

// ETA to waypoints

// Calculate distance and ETA to a coordinate
func calculateDistanceAndETA(currentLocation: CLLocationCoordinate2D, targetLocation: CLLocationCoordinate2D, speed: Double) -> (distance: CLLocationDistance, eta: TimeInterval) {
    let currentLocation = CLLocation(latitude: currentLocation.latitude, longitude: currentLocation.longitude)
    let targetLocation = CLLocation(latitude: targetLocation.latitude, longitude: targetLocation.longitude)
    
    let distance = currentLocation.distance(from: targetLocation)
    let eta = distance / speed
    
    return (distance, eta)
}

func timeRemaining() -> String {
    let calendar = Calendar.current
    
    // Hardcode the future date to 09/23/23 8:00am EST
    var futureDateComponents = DateComponents()
    futureDateComponents.year = 2023
    futureDateComponents.month = 9
    futureDateComponents.day = 23
    futureDateComponents.hour = 8
    futureDateComponents.minute = 0
    futureDateComponents.second = 0
    futureDateComponents.timeZone = TimeZone(identifier: "America/New_York") // EST
    
    guard let futureDate = calendar.date(from: futureDateComponents) else {
        return "Invalid future date!"
    }
    
    let currentDate = Date()
    
    // Check if the future date is in the past
    if futureDate < currentDate {
        return "The future date is in the past!"
    }
    
    // Calculate the difference between the two dates
    let components = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: futureDate)
    
    // Build a string to represent the time remaining
    var timeRemainingString = ""
    
    if let days = components.day, days > 0 {
        timeRemainingString += "\(days) days "
    }
    
    if let hours = components.hour, hours > 0 {
        timeRemainingString += "\(hours) hours "
    }
    
    if let minutes = components.minute, minutes > 0 {
        timeRemainingString += "\(minutes) minutes "
    }
    
    if let seconds = components.second, seconds > 0 {
        timeRemainingString += "\(seconds) seconds "
    }
    
    return timeRemainingString.isEmpty ? "The future date is now!" : "Time remaining: \(timeRemainingString)"
}
