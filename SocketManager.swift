import Foundation
import SocketIO

class SocketIOManager: ObservableObject {
    
    // singleton
    static let shared = SocketIOManager()
    
    var manager: SocketManager!
    var socket: SocketIOClient!
    
    @Published var message: String = ""
    @Published var connectionStatus = "disconnected"
    
    private init() {
        debugLogger(message: "Socket Manager initilizing...")
        setup()
    }
    
    func setup() {
        print("setting up socket connection...")
#if DEBUG
        let urlString = "http://localhost:8080"
#else
        let urlString = "https://swim-server-a39ef0ff437c.herokuapp.com"
#endif
        manager = SocketManager(socketURL: URL(string: urlString)!)
        socket = manager.defaultSocket
        
        // connect event handler
        socket.on(clientEvent: .connect) { data, ack in
            debugLogger(message: "SOCKETMANAGER: Connected")
            self.connectionStatus = "connected"
        }
        
        socket.on(clientEvent: .disconnect) {data, ack in
            debugLogger(message: "SOCKETMANAGER: disconnected")
            self.connectionStatus = "disconnected"
        }
        
        socket.on("startTracking") { data, ack in
            debugLogger(message: "SOCKETMANAGER: Remote request to START tracking locations received...")
            LocationManager.shared.startLocation()
        }
        
        socket.on("stopTracking") { data, ack in
            debugLogger(message: "SOCKETMANAGER: Remote request to STOP tracking locations received...")
            LocationManager.shared.stopLocation()
        }
        
        
        socket.connect()
    }
}
