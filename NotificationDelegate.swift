//
//  Notifications.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/10/23.
//

import Foundation
import UserNotifications
import UIKit

class NotificationDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        
        debugLogger(message: "Device Token: \(token)")
        // persist the token so we can register it to the server
        UserDefaults.standard.set(token, forKey: "notificationToken")
        
        UserProfileService.shared.persistToken(token: token) { result in
            switch result {
            case .success(let data):
                // Handle success
                print("Token persisted successfully: \(data)")
            case .failure(let error):
                // Handle failure
                print("Failed to persist token: \(error)")
            }
        }
        
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for remote notifications: \(error)")
    }
}
