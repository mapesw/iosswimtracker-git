//
//  NetworkManager.swift
//  Step Up Swim Tracker
//
//  Created by William Mapes on 9/8/23.
//

import Foundation



class NetworkManager {
    
    // Singleton instance
    static let shared = NetworkManager()
    
    private init() {}
    
    // GET request
    func getRequest(url: String, completion: @escaping (Data?, Error?) -> Void) {
        
        guard let url = URL(string: url) else {
            completion(nil, NSError(domain: "Invalid URL", code: 1, userInfo: nil))
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            completion(data, error)
        }
        task.resume()
    }
    
    // POST location to Server
    func postLocationData(completion: @escaping (Data?, Error?) -> Void) {
        
#if DEBUG
        let urlString = "http://localhost:8080/api/location"
#else
        let urlString = "https://swim-server-a39ef0ff437c.herokuapp.com/api/location"
#endif
        
        // Fetch location data from memory
        getLocationDataFromMemory { result in
            
            switch result {
                // if success, set locationData variable
            case .success(let locationData):
                
                // Setup URL
                guard let url = URL(string: urlString) else {
                    completion(nil, NSError(domain: "Invalid URL", code: 1, userInfo: nil))
                    return
                }
                // Setup request
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                // encode the JSON
                do {
                    let jsonData = try JSONEncoder().encode(locationData)
                    
                    // set JSON in request body
                    request.httpBody = jsonData
                } catch {
                    completion(nil, NSError(domain: "JSON serialization error", code: 1, userInfo: nil))
                    return
                }
                
                // send it
                let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                    completion(data, error)
                }
                
                task.resume()
                
            case .failure(let error):
                // Handle error
                completion(nil, error)
            }
        }
    }
}
